package br.com.eutectum.scenes.timer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import br.com.eutectum.databinding.FormTimerBinding
import br.com.eutectum.utils.MaskUtil.HOURS
import br.com.eutectum.utils.insertMask
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class FormTimerFragment : Fragment() {

    private val timerViewModel: FormTimerViewModelContract by sharedViewModel<TimerViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FormTimerBinding.inflate(inflater, container, false)
        binding.expectedTimeEDT.insertMask(HOURS)
        binding.breakTimeEDT.insertMask(HOURS)
        binding.betweenIntervalsEDT.insertMask(HOURS)
        binding.viewModel = timerViewModel
        binding.lifecycleOwner = this
        subscribeToViewModel()
        return binding.root
    }

    private fun subscribeToViewModel() {
//        with(viewModel) {
//            data.observe(viewLifecycleOwner, Observer {
////                val bundle = Bundle()
////                bundle.putParcelable(FORM_TIMER_MODEL_KEY, it)
////                val intent = Intent(viewLifecycleOwner, TimerFragment::class.java)
////                intent.putExtras(bundle)
////                startActivity(intent)
//            })
//            error.observe(viewLifecycleOwner, Observer { errorMessage ->
//                context?.let {
//                    AlertDialog.Builder(it).setMessage(errorMessage).create().show()
//                }
//            })
//        }
    }

}
