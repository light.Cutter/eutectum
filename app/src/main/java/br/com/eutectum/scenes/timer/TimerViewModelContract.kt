package br.com.eutectum.scenes.timer

import androidx.lifecycle.LiveData

interface TimerViewModelContract {
    val currentTime: LiveData<String>
    fun startTimer()
}