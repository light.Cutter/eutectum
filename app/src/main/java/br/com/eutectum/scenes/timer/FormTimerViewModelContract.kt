package br.com.eutectum.scenes.timer

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

interface FormTimerViewModelContract {
    val expectedTime: MutableLiveData<String>
    val expectedTimeError: LiveData<String>
    val breakTime: MutableLiveData<String>
    val breakTimeError: LiveData<String>
    val betweenIntervals: MutableLiveData<String>
    val betweenIntervalsError: LiveData<String>
    val isStartButtonEnabled: LiveData<Boolean>
    fun onStartClick()
}