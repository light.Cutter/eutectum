package br.com.eutectum.scenes.timer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import br.com.eutectum.databinding.TimerBinding
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class TimerFragment : Fragment() {

    private val timerViewModel: TimerViewModelContract by sharedViewModel<TimerViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = TimerBinding.inflate(inflater, container, false)
        binding.viewModel = timerViewModel
        binding.lifecycleOwner = this
        return binding.root
    }
}
