package br.com.eutectum.scenes.timer

import android.view.animation.Transformation
import androidx.lifecycle.*

class TimerViewModel : ViewModel(), FormTimerViewModelContract, TimerViewModelContract {

    private val _expectedTime = MutableLiveData<String>()
    private val _breakTime = MutableLiveData<String>()
    private val _betweenIntervals = MutableLiveData<String>()

    private val _currentTime = MutableLiveData<String>()

    // Form Timer View Model
    override val expectedTimeError: LiveData<String>
    override val breakTimeError: LiveData<String>
    override val betweenIntervalsError: LiveData<String>

    override val expectedTime: MutableLiveData<String>
        get() = _expectedTime
    override val breakTime: MutableLiveData<String>
        get() = _breakTime
    override val betweenIntervals: MutableLiveData<String>
        get() = _betweenIntervals

    override val isStartButtonEnabled: LiveData<Boolean>

    // Timer View Model
    override val currentTime: LiveData<String>
        get() = _currentTime

    private var timeErrorMessage = "Horário inválido"

    init {
        isStartButtonEnabled = MediatorLiveData<Boolean>().apply {
            val isExpectedTimeValid = Transformations.map(expectedTime) { isTimeValid(it) }
            val isBreakTimeValid = Transformations.map(breakTime) { isTimeValid(it) }
            val isBetweenIntervalsValid = Transformations.map(betweenIntervals) { isTimeValid(it) }
            expectedTimeError = Transformations.map(isExpectedTimeValid) { if(!it) timeErrorMessage else "" }
            breakTimeError = Transformations.map(isBreakTimeValid) { if(!it) timeErrorMessage else "" }
            betweenIntervalsError = Transformations.map(isBetweenIntervalsValid) { if(!it) timeErrorMessage else "" }
            addSource(isExpectedTimeValid) {
                value = it ?: false && isBreakTimeValid.value ?: false && isBetweenIntervalsValid.value ?: false
            }
            addSource(isBreakTimeValid) {
                value = it ?: false && isExpectedTimeValid.value ?: false && isBetweenIntervalsValid.value ?: false
            }
            addSource(isBetweenIntervalsValid) {
                value = it ?: false && isExpectedTimeValid.value ?: false && isBreakTimeValid.value ?: false
            }
        }
    }

    // Form Timer View Model
    override fun onStartClick() {  }

    // Timer View Model
    override fun startTimer() {
//        val expectedTime = expectedTime?.hourToMillis() ?: 0L
//        val timer = object: CountDownTimer(expectedTime, 1000) {
//            override fun onTick(millisUntilFinished: Long) {
//                _currentTime.postValue(millisUntilFinished.millisToHours())
//            }
//
//            override fun onFinish() {
//
//            }
//        }
//        timer.start()
    }

    private fun isTimeValid(time: String?) : Boolean = time?.
            // take only if has length 5
            takeIf { it.length == 5 }?.
            // separated by :
            split(":")?.
            // if are invalid hour values
            takeIf { (hour, minute) -> hour.toInt() <= 12 || minute.toInt() <= 59 }?.
            // transform into error message, otherwise empty string
            let { true } ?: let { false }

}