package br.com.eutectum.scenes.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import br.com.eutectum.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
