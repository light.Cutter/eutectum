package br.com.eutectum.utils

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import android.widget.TextView

interface MaskCallBack {
    fun onMaxTextLength(text: String) {}
    fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
    fun afterTextChanged(s: Editable) {}
}

fun TextView.insertMask(maskType: String) {
    val str = this.text.toString()
    val mask = MaskUtil.insert(str, maskType)
    this.text = mask
}

fun EditText.insertMask(maskType: String, textView: TextView? = null, callback: MaskCallBack? = null) {
    this.addTextChangedListener(MaskUtil.insert(this, maskType, textView, callback))
}

fun EditText.removeMask() {
    MaskUtil.remove(this)
}

fun String.insertMask(maskType: String): String {
    val str = this.unmask()
    return MaskUtil.insert(str, maskType)
}

fun String.unmask(): String {
    return this.replace("[^0-9]*".toRegex(), "")
}

object MaskUtil {
    private val TAG = this::class.java.simpleName

    const val CPF_MASK = "###.###.###-##"
    const val CNPJ_MASK = "##.###.###/####-##"
    const val BIRTH_MASK = "##/##/####"
    const val PHONE_MASK = "(##)#####-####"
    const val CARD_NUMBER_MASK = "#### #### #### ####"
    const val CARD_DATE_MASK = "##/##"
    const val CARD_CVV_MASK = "###"
    const val ADDRESS_ZIP_CODE_MASK = "#####-###"
    const val HOURS = "##:##"

    var textWatchers: ArrayList<TextWatcher> = arrayListOf()
    var isUpdating: Boolean = false
    var oldValue = ""

    private fun unmask(s: String): String = s.replace("[^0-9]*".toRegex(), "")

    fun remove(editText: EditText) {
        if (textWatchers.isNotEmpty()) {
            editText.removeTextChangedListener(textWatchers.last())
        }
    }

    fun insert(str: String, maskType: String): String {
        var mask = ""
        var cont = 0

        if (str.isNotBlank()) {
            maskType.toCharArray().forEach maskLoop@{ char ->
                if (char != '#' && str.length > cont) {
                    mask += char
                    return@maskLoop
                }
                mask += if (str.length > cont) str.toCharArray()[cont] else ""
                cont++
            }
        }
        return mask
    }

    private fun onTextChanged(maskType: String, s: CharSequence): String {
        val value = unmask(s.toString())
        val mask: String = maskType

        var maskAux = ""
        if (isUpdating) {
            oldValue = value
            isUpdating = false
            return value
        }
        var i = 0
        for (character in mask.toCharArray()) {
            if (character != '#' && value.length > oldValue.length || character != '#' && value.length < oldValue.length && value.length != i) {
                maskAux += character
                continue
            }

            try {
                maskAux += value[i]
            } catch (e: Exception) {
                break
            }

            i++
        }
        return maskAux
    }

    private fun doOnTextChange(s: CharSequence, editText: EditText, maskType: String, textView: TextView? = null) {
        try {
            if (!isUpdating && unmask(s.toString()) != oldValue && unmask(s.toString()).length > oldValue.length) {
                val result = this@MaskUtil.onTextChanged(maskType, s)
                isUpdating = true
                editText.apply {
                    setText(result)
                    setSelection(result.length)
                }
                oldValue = unmask(result)
                textView?.text = result
            } else {
                oldValue = unmask(s.toString())
                isUpdating = false
            }
        } catch (ex: java.lang.Exception) {
            Log.e(TAG, "An error occurred when trying get length on string")
        }
    }

    internal fun insert(editText: EditText, maskType: String, textView: TextView? = null, callback: MaskCallBack? = null): TextWatcher {
        isUpdating = false
        oldValue = ""
        val textWatcher = object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                doOnTextChange(s, editText, maskType, textView)
                callback?.onTextChanged(s, start, before, count)
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                callback?.beforeTextChanged(s, start, count, after)
            }

            override fun afterTextChanged(s: Editable) {
                callback?.afterTextChanged(s)
                if (s.length == getSize(maskType)) callback?.onMaxTextLength(s.toString())
            }
        }
        textWatchers.add(textWatcher)
        return textWatchers.last()
    }

    internal fun changeMask(currentMask: String, editText: EditText, changeMask: (nextMask: Boolean) -> String?) {
        editText.apply {
            removeMask()
            insertMask(currentMask, null, object : MaskCallBack {
                override fun onMaxTextLength(text: String) {
                    val mask = changeMask(true)
                    mask?.let {
                        removeMask()
                        changeMask(it, editText, changeMask)
                    }
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    val updated = s.toString()
                    if (before == 1 && updated.length + 1 <= currentMask.length) {
                        val mask = changeMask(false)
                        mask?.let {
                            removeMask()
                            changeMask(it, editText, changeMask)
                        }
                    }
                }

            })
        }
    }

    fun getSize(mask: String) = mask.length

    fun getSizeUnmask(mask: String): Int {
        val char = '#'
        var count = 0
        for (i in 0 until mask.length) {
            if (mask.get(i) == char) count++
        }
        return count
    }
}
