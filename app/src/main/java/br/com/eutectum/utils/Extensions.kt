package br.com.eutectum.utils

import java.util.concurrent.TimeUnit

fun String.hourToMillis(): Long {
    val (hours, minutes) = this.split(":")
    return (hours.toLong() * 360000L) + (minutes.toLong() * 60000L)
}

fun String.formatToHour(): String {
    var num = replace("/[^0-9]/g".toRegex(), "")
    num = num.replace("/(\\d{2})(\\d{1,2})$/".toRegex(), "$1:$2")
    return num
}

fun Long.millisToHours(): String {
    val millis = this
    val minutes = TimeUnit.MILLISECONDS.toMinutes(millis)
    val hours = (minutes / 60).toInt()
    val resultingMinutes = minutes.rem(60).toInt()

    return "$hours:$resultingMinutes"
}