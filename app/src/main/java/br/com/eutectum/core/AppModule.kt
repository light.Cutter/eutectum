package br.com.eutectum.core

import br.com.eutectum.scenes.timer.TimerViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    viewModel { TimerViewModel() }
}